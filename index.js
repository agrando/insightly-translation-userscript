// ==UserScript==
// @name         Insightly Translations
// @namespace    https://agrando.com
// @version      0.6
// @description  Script to retrieve translations from a google sheet and replace german with french strings in Insightly
// @author       Sebastian Rehm
// @match        https://crm.na1.insightly.com/*
// @downloadURL  https://gitlab.com/agrando/insightly-translation-userscript/-/raw/master/index.js
// @updateURL    https://gitlab.com/agrando/insightly-translation-userscript/-/raw/master/index.js
// @grant        none
// ==/UserScript==

const googleSheetsUrl =
  'https://gsx2json.com/api?id=17m2zolPrV0cskaADGIy521VKADYo5r5IYJppfoi_MOo&sheet=1'

let translations = []

const fetchTranslations = async () => {
  const googleSheet = await fetch(googleSheetsUrl)
  const jsonData = await googleSheet.json()
  translations = jsonData.rows
}

const setupMutationObserver = (targetNode) => {
  const config = { attributes: false, childList: true, subtree: true }
  // defer execution since changes often happen right after each other
  let deferTimeout = null
  const callback = () => {
    clearTimeout(deferTimeout)
    deferTimeout = setTimeout(() => replaceStrings(targetNode), 200)
  }
  const observer = new MutationObserver(callback)
  observer.observe(targetNode, config)
}

const replaceStrings = (node) => {
  const elms = node.getElementsByTagName('*')
  const len = elms.length
  const translationLength = translations.length

  // For loops are generally the fastest loops in most Benchmarks
  for (let ii = 0; ii < len; ii++) {
    const childElms = elms[ii].childNodes
    const len2 = childElms.length

    for (let jj = 0; jj < len2; jj++) {
      const node = childElms[jj]

      // NOTE: We have to exclude options fields here since Insightly is not using the value
      //       attribute but relying on the text of the option
      if (node.nodeType === 3 && node.parentNode.tagName !== 'OPTION') {
        for (let yy = 0; yy < translationLength; yy++) {
          const translation = translations[yy]

          if (translation.fr && node.nodeValue.trim() === translation.de) {
            node.nodeValue = translation.fr
          }
        }
      }
    }
  }
}

;(function () {
  'use strict'
  const observedIds = ['modal-details', 'modal-form', 'main']
  const observedNodes = observedIds.map((id) => document.getElementById(id))
  observedNodes.forEach((node) => setupMutationObserver(node))

  fetchTranslations().then(() => {
    observedNodes.forEach((node) => replaceStrings(node))
  })
})()
