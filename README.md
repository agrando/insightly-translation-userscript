# Insightly Translation Userscript

## Purpose
This script was build to translate custom labels / objects etc inside Insightly, since only standard UI is automatically translated through switching languages.

## Functionality
Right now the script is capable of translating all German strings provided inside a Google Sheet into the provided French translations. The Google Sheet for translations can be found [here](https://docs.google.com/spreadsheets/d/1egYOcDZwWyKs30MDNemwmWGyoL5cdPCUpCm4Eapre_c/edit#gid=0).

## Installation
To get the script running do the following steps:

* Install Tampermonkey from [here](https://www.tampermonkey.net/index.php?ext=dhdg&browser=chrome).
* Make sure Tampermonkey is pinned as an extension in your Chrome (puzzle icon on the upper right corner)
* Click on the Tampermonkey Icon and click "create new script"
* Replace the content with the contents of the index.js in this repo and save the file.
* The script should automatically work on relevant insightly sites

