module.exports = {
  parserOptions: {
    parser: "babel-eslint",
    ecmaVersion: 2017,
    sourceType: "module"
  },

  env: {
    browser: true,
    commonjs: true,
    es6: true
  },

  plugins: ["prettier"],

  extends: ["eslint:recommended", "plugin:prettier/recommended"],

  rules: {
    "no-console": "error",
    "spaced-comment": ["error", "always"],
    "no-else-return": ["error", { allowElseIf: false }],
    "no-multi-spaces": ["error", { ignoreEOLComments: false }],
    "no-multi-str": "error",
    "no-undefined": "error",
    "handle-callback-err": "error",
    "no-path-concat": "error",
    "no-var": "error",
    "prefer-const": "error",
    "prefer-template": "error",
    "no-alert": "error",
    "array-bracket-spacing": ["error", "never"],
    "object-curly-spacing": ["error", "always"],
    curly: "error",
    "prettier/prettier": ["error", { singleQuote: true, semi: false }]
  }
};
